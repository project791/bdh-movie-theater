import { LayoutTheme } from "../HOC/layout";
import Page404 from "../pages/404Page/Page404";
import ChiTietPhim from "../pages/ChiTietPhim/ChiTietPhim";
import HeThongRap from "../pages/HeThongRap/HeThongRap";
import KhuyenMai from "../pages/KhuyenMai&SuKien/KhuyenMai";
import SuKien from "../pages/KhuyenMai&SuKien/SuKien";
import LichChieu from "../pages/LichChieu/LichChieu";
import TrangChu from "../pages/TrangChu/TrangChu";
import TrangDangKy from "../pages/TrangDangKy/TrangDangKy";
import TrangDangNhap from "../pages/TrangDangNhap/TrangDangNhap";
import TrangDatVe from "../pages/TrangDatVe/TrangDatVe";
import TrangXacNhanVe from "../pages/TrangXacNhanVe/TrangXacNhanVe";
export const userRoute = [
  {
    path: "/",
    component: <LayoutTheme Component={TrangChu} />,
    isUseLayout: true,
    exact: true,
  },
  {
    path: "/lichchieu",
    component: <LayoutTheme Component={LichChieu} />,
    isUseLayout: true,
    exact: true,
  },
  {
    path: "/hethongrap",
    component: <LayoutTheme Component={HeThongRap} />,
    isUseLayout: true,
  },
  {
    path: "/sukien",
    component: <LayoutTheme Component={SuKien} />,
    isUseLayout: true,
    exact: true,
  },
  {
    path: "/khuyenmai",
    component: <LayoutTheme Component={KhuyenMai} />,
    isUseLayout: true,
    exact: true,
  },
  {
    path: "/chitiet/:maPhim",
    component: <LayoutTheme Component={ChiTietPhim} />,
    isUseLayout: true,
    exact: true,
  },
  {
    path: "/dangnhap",
    component: <LayoutTheme Component={TrangDangNhap} />,
    isUseLayout: true,
    exact: true,
  },
  {
    path: "/dangky",
    component: <LayoutTheme Component={TrangDangKy} />,
    isUseLayout: true,
    exact: true,
  },
  {
    path: "/xacnhan",
    component: <LayoutTheme Component={TrangXacNhanVe} />,
    isUseLayout: true,
    exact: true,
  },

  {
    path: "/datVe/:maLichChieu",
    component: <LayoutTheme Component={TrangDatVe} />,
    isUseLayout: true,
    exact: true,
  },
  {
    path: "*",
    component: Page404,
    exact: true,
  },
];
