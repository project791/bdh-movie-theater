import { movieService } from "../../services/MovieService";
import { userService } from "../../services/UserService";
import {
  BOOK_TICKET,
  CLOSE_TRAILER,
  CONFIRM_TICKET,
  OPEN_TRAILER,
  SELECT_SEAT,
  SET_USER_INFOR,
  START_LOADING,
} from "../constants/constans";
export const getMovieListActionService = () => {
  return movieService.getMovieList();
};
export const setUserInforAction = (user) => {
  return {
    type: SET_USER_INFOR,
    payload: user,
  };
};
export const setUserInforActionService = (
  dataLogin = {},
  handleLSuccess = () => {},
  handleFail = () => {}
) => {
  /* thực hiện thêm các tác vụ bất đồng bộ
  (lấy, xử lý dữ liệu) trước khi gửi đến redux reducer*/

  return (dispatch) => {
    userService
      .postDangNhap(dataLogin)
      .then((res) => {
        handleLSuccess();
        dispatch({
          type: SET_USER_INFOR,
          payload: res.data.content,
        });
      })
      .catch((err) => {
        handleFail();
      });
  };
};
export const handleOpenTrailer = (url) => {
  return (dispatch) => {
    dispatch({
      type: OPEN_TRAILER,
      payload: url,
    });
  };
};
export const handleCloseTrailer = () => {
  return (dispatch) => {
    dispatch({
      type: CLOSE_TRAILER,
    });
  };
};
export const handleSelectSeat = (maGhe) => {
  return (dispatch) => {
    dispatch({
      type: SELECT_SEAT,
      payload: maGhe,
    });
  };
};
export const handleBookingTicket = (data) => {
  return {
    type: BOOK_TICKET,
    payload: data,
  };
};
export const handleConfirmTicket = (data) => {
  return {
    type: CONFIRM_TICKET,
    payload: data,
  };
};
export const handleStartLoading = (boolean) => {
  return {
    type: START_LOADING,
    payload: boolean,
  };
};
