import { localStorageService } from "../../services/LocalStorage";
import {
  BOOK_TICKET,
  CANCEL_SEAT,
  CLOSE_TRAILER,
  CONFIRM_TICKET,
  END_LOADING,
  OPEN_TRAILER,
  SELECT_SEAT,
  SET_USER_INFOR,
  START_LOADING,
} from "../constants/constans";

const initialState = {
  userInfor: localStorageService.getUserInfor(),
  trailer: {
    isOpen: false,
    id: "",
  },
  selectedSeat: [],
  confirmTicket: {},
  isLoading: false,
};
export const userReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case SET_USER_INFOR: {
      state.userInfor = payload;
      return { ...state };
    }
    case OPEN_TRAILER: {
      let newId = payload.replace("https://www.youtube.com/watch?v=", "");
      newId = newId.replace("https://www.youtube.com/embed/", "");
      newId = newId.replace("https://youtu.be/", "");
      state.trailer = { isOpen: true, id: newId };
      return { ...state };
    }
    case CLOSE_TRAILER: {
      state.trailer = { isOpen: false, id: "" };
      return { ...state };
    }
    case SELECT_SEAT: {
      let cloneSelectedSeat = state.selectedSeat;
      let index = cloneSelectedSeat.findIndex((item) => {
        return item.maGhe === payload.maGhe;
      });
      let newPayload = { ...payload, reserved: false };
      if (index === -1) {
        cloneSelectedSeat.push(newPayload);
      } else {
        cloneSelectedSeat.splice(index, 1);
      }
      payload.reserved = !payload.reserved;
      state.selectedSeat = cloneSelectedSeat;
      let json = JSON.stringify(state);
      return { ...JSON.parse(json) };
    }
    case CONFIRM_TICKET: {
      state.confirmTicket = payload;
      let json = JSON.stringify(state);
      return { ...JSON.parse(json) };
    }
    case START_LOADING: {
      state.isLoading = payload;
      let json = JSON.stringify(state);
      return { ...JSON.parse(json) };
    }
    default:
      return state;
  }
};
