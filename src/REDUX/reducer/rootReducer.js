import { applyMiddleware, combineReducers, compose, createStore } from "redux";
import thunk from "redux-thunk";
import { userReducer } from "./userReducer";

export const rootReducer = combineReducers({ userReducer });
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
export const storeRedux = createStore(
  rootReducer,
  composeEnhancers(applyMiddleware(thunk))
);
