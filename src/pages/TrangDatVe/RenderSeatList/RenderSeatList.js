import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { message } from "antd";
import {
  handleConfirmTicket,
  handleSelectSeat,
} from "../../../REDUX/action/action";
import { movieService } from "../../../services/MovieService";
import { useHistory } from "react-router-dom";
export default function RenderSeatList({ data }) {
  let history = useHistory();
  let selectedSeat = useSelector((state) => {
    return state.userReducer.selectedSeat;
  });
  let userInfor = useSelector((state) => {
    return state.userReducer.userInfor;
  });
  let dispatch = useDispatch();
  const vnd = (number) => {
    return (number * 1)
      .toLocaleString("it-IT", {
        style: "currency",
        currency: "VND",
      })
      .replace("VND", "đ");
  };
  const bookingTicket = () => {
    if (userInfor == null) {
      message.error("Bạn chưa đăng nhập");
    } else if (selectedSeat.length === 0) {
      message.error("Bạn chưa chọn ghế");
    } else {
      let newSeatInfor = {
        maLichChieu: data.thongTinPhim.maLichChieu,
        danhSachVe: selectedSeat,
      };
      movieService
        .postDatVe(newSeatInfor)
        .then((res) => {
          dispatch(handleConfirmTicket(data));
          message.success("Đặt vé thành công");
          setTimeout(() => {
            history.push("/xacnhan");
          }, 1000);
        })
        .catch((err) => {
          message.error("Thất bại , Thử lại !!!");
        });
    }
  };
  return (
    <div className="flex w-full">
      <div className="w-3/5">
        <p className=" w-full text-xl mb-3 text-center text-white">SCREEN</p>
        <div className=" container_perspective w-full">
          <div className="screen_datVe "></div>
        </div>
        <div className=" w-full text-white grid danhSachGhe gap-2 font-medium mt-9">
          {data?.danhSachGhe?.map((ghe) => {
            if (ghe.loaiGhe == "Vip" && ghe.daDat === false) {
              if (ghe.reserved === true) {
                return (
                  <div
                    key={ghe.maGhe}
                    className="text-center seat px-3 py-3  rounded cursor-pointer"
                    style={{ backgroundColor: "#54ab34" }}
                    onClick={() => {
                      dispatch(handleSelectSeat(ghe));
                    }}
                  >
                    {ghe.tenGhe}
                  </div>
                );
              } else {
                return (
                  <div
                    key={ghe.maGhe}
                    className="text-center px-3 py-3 bg-yellow-500 rounded cursor-pointer"
                    onClick={() => {
                      dispatch(handleSelectSeat(ghe));
                    }}
                  >
                    {ghe.tenGhe}
                  </div>
                );
              }
            }
            if (ghe.loaiGhe == "Thuong" && ghe.daDat === false) {
              if (ghe.reserved === true) {
                return (
                  <div
                    key={ghe.maGhe}
                    className="text-center px-3 py-3 seat  rounded cursor-pointer"
                    style={{ backgroundColor: "#54ab34" }}
                    onClick={() => {
                      dispatch(handleSelectSeat(ghe));
                    }}
                  >
                    {ghe.tenGhe}
                  </div>
                );
              } else {
                return (
                  <div
                    key={ghe.maGhe}
                    className="text-center px-3 py-3 seat bg-gray-600 rounded cursor-pointer"
                    onClick={() => {
                      dispatch(handleSelectSeat(ghe));
                    }}
                  >
                    {ghe.tenGhe}
                  </div>
                );
              }
            }
            if (ghe.loaiGhe == "Thuong" && ghe.daDat === true) {
              return (
                <div
                  key={ghe.maGhe}
                  className="text-center px-3 py-3 seat bg-red-600 rounded cursor-not-allowed"
                >
                  X
                </div>
              );
            }
            if (ghe.loaiGhe == "Vip" && ghe.daDat === true) {
              return (
                <div
                  key={ghe.maGhe}
                  className="text-center px-3 py-3 seat bg-red-600 rounded  cursor-not-allowed"
                >
                  X
                </div>
              );
            }
          })}
        </div>
      </div>
      <div className="w-96 mx-auto">
        <div className=" form_datVe px-8 py-5">
          <div className="w-full">
            <div className="mb-5">
              <h2 className="text-white font-medium text-xl text-center">
                VÉ ĐANG ĐẶT
              </h2>
            </div>
            <div className=" w-full">
              <div className="grid grid-cols-2">
                <p className=" text-white text-xl">TÊN PHIM :</p>
                <p
                  className=" uppercase font-medium text-xl"
                  style={{ color: "#54ab34" }}
                >
                  {data?.thongTinPhim?.tenPhim}
                </p>
              </div>
              <div className="grid grid-cols-2 my-5">
                <p className=" text-white text-xl">RẠP :</p>
                <p
                  className=" uppercase font-medium text-xl"
                  style={{ color: "#54ab34" }}
                >
                  {data?.thongTinPhim?.tenRap}
                </p>
              </div>
              <div className="grid grid-cols-2">
                <p className=" text-white text-xl">SỐ GHẾ BẠN CHỌN :</p>
                <div>
                  <p
                    className=" break-words font-medium text-xl"
                    style={{ color: "#54ab34" }}
                  >
                    {selectedSeat
                      ?.map((item) => {
                        return `${item.stt}`;
                      })
                      .join(",")}
                  </p>
                </div>
              </div>
              <div className="grid grid-cols-2 my-5">
                <p className=" text-white text-xl">TỔNG TIỀN :</p>
                <p
                  className=" uppercase font-medium text-xl"
                  style={{ color: "#54ab34" }}
                >
                  {vnd(
                    selectedSeat?.reduce((total, seat) => {
                      return total + seat.giaVe;
                    }, 0)
                  )}
                </p>
              </div>
            </div>
            <div className=" flex justify-end gap-2">
              <div
                className=" py-3 px-4  text-white font-medium rounded cursor-pointer"
                style={{ backgroundColor: "#54ab34" }}
                onClick={() => bookingTicket()}
              >
                ĐẶT VÉ
              </div>
            </div>
          </div>
        </div>
        <div className=" grid grid-cols-2 gap-5 mt-5">
          <div className="flex">
            <div className="text-center px-5 py-3  bg-red-600 rounded cursor-pointer text-white mr-3">
              X
            </div>
            <p className="text-white font-medium mt-2 ">Ghế đã có người đặt</p>
          </div>
          <div className="flex">
            <div className="text-center px-4 py-3  bg-gray-600 rounded cursor-pointer text-white mr-3">
              00
            </div>
            <p className="text-white font-medium mt-2 ">Ghế thường</p>
          </div>
          <div className="flex">
            <div className="text-center px-4 py-3  bg-yellow-500 rounded cursor-pointer text-white mr-3">
              00
            </div>
            <p className="text-white font-medium mt-2 ">Ghế vip</p>
          </div>
          <div className="flex">
            <div
              className="text-center px-4 py-3   rounded cursor-pointer text-white mr-3"
              style={{ backgroundColor: "#54ab34" }}
            >
              00
            </div>
            <p className="text-white font-medium mt-2 ">Ghế đang chọn</p>
          </div>
        </div>
      </div>
    </div>
  );
}
