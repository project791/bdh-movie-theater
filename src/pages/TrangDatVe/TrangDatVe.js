import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { movieService } from "../../services/MovieService";
import RenderSeatList from "./RenderSeatList/RenderSeatList";

export default function TrangDatVe() {
  let { maLichChieu } = useParams();
  const [dataLichChieu, setDataLichChieu] = useState({});
  useEffect(() => {
    movieService
      .getDanhSachPhongVe(maLichChieu)
      .then((res) => {
        setDataLichChieu(res.data.content);
      })
      .catch((err) => {});
    window.scrollTo(0, 0);
  }, []);

  return (
    <div className="w-full">
      <div
        className="mx-auto px-24 py-10"
        style={{
          backgroundImage:
            "url(https://www.bhdstar.vn/wp-content/themes/bhd/assets/images/movie-details-bg.jpg)",
        }}
      >
        <div className="flex">
          <img
            src={dataLichChieu?.thongTinPhim?.hinhAnh}
            alt=""
            className=" w-36 h-42 rounded"
            style={{ border: "2px solid #54ab35 " }}
          />
          <div className=" text-white  ml-8 uppercase leading-9">
            <p className="text-2xl font-medium" style={{ color: "#54ab35" }}>
              {dataLichChieu?.thongTinPhim?.tenPhim}
            </p>
            <p>Ngày Chiếu : {dataLichChieu?.thongTinPhim?.ngayChieu}</p>
            <p>Giờ Chiếu : {dataLichChieu?.thongTinPhim?.gioChieu}</p>
            <p>{dataLichChieu?.thongTinPhim?.tenCumRap}</p>
            <p>{dataLichChieu?.thongTinPhim?.diaChi}</p>
          </div>
        </div>
        <RenderSeatList data={dataLichChieu} />
      </div>
    </div>
  );
}
