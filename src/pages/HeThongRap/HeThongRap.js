import React from "react";

export default function HeThongRap() {
  return (
    <div
      className="w-full mx-auto py-20"
      style={{
        backgroundImage:
          "url(https://www.bhdstar.vn/wp-content/themes/bhd/assets/images/bg-cinema-10.png)",
      }}
    >
      <div className="container mx-auto">
        <div className=" px-10">
          <h1 className="text-4xl font-medium text-center mb-10 text-white">
            HỆ THỐNG RẠP
          </h1>
          <div className=" flex justify-evenly">
            <div className=" w-max h-max">
              <img
                src="https://www.bhdstar.vn/wp-content/uploads/2021/12/long-khanh-243x330.jpg"
                alt=""
                className=" w-64 h-96 event_item hover:scale-110  "
              />
              <div
                className=" w-full h-24 px-2 py-3"
                style={{
                  backgroundColor: "#172029",
                  border: "3px solid #207f2b",
                }}
              >
                <h2 className=" text-white font-medium hover:text-yellow-500  transition duration-200 cursor-pointer">
                  BHD STAR LONG KHÁNH
                </h2>
                <div className=" flex mt-4 justify-around gap-4 ">
                  <p style={{ color: "#8b9aaa" }}>97.559 Thích</p>
                  <div
                    className=" py-2 px-5 rounded font-medium cursor-pointer"
                    style={{ backgroundColor: "#3b5998", color: "#a9b7c4" }}
                  >
                    Chia sẻ
                  </div>
                </div>
              </div>
            </div>
            <div className=" w-max h-max">
              <img
                src="https://www.bhdstar.vn/wp-content/uploads/2019/12/GARDEN-243x330.jpg"
                alt=""
                className=" w-64 h-96 event_item hover:scale-110"
              />
              <div
                className=" w-full h-24 px-2 py-3"
                style={{
                  backgroundColor: "#172029",
                  border: "3px solid #207f2b",
                }}
              >
                <h2 className=" text-white font-medium cursor-pointer hover:text-yellow-500  transition duration-200">
                  BHD STAR THE GARDEN
                </h2>
                <div className=" flex mt-4 justify-around gap-4 ">
                  <p style={{ color: "#8b9aaa" }}>179.284 Thích</p>
                  <div
                    className=" py-2 px-5 rounded font-medium cursor-pointer"
                    style={{ backgroundColor: "#3b5998", color: "#a9b7c4" }}
                  >
                    Chia sẻ
                  </div>
                </div>
              </div>
            </div>
            <div className=" w-max h-max">
              <img
                src="https://www.bhdstar.vn/wp-content/uploads/2018/04/Cau-giay-copy-243x330.jpg"
                alt=""
                className=" w-64 h-96 event_item hover:scale-110"
              />
              <div
                className=" w-full h-24 px-2 py-3"
                style={{
                  backgroundColor: "#172029",
                  border: "3px solid #207f2b",
                }}
              >
                <h2 className=" text-white font-medium cursor-pointer hover:text-yellow-500  transition duration-200">
                  BHD STAR DISCOVERY
                </h2>
                <div className=" flex mt-4 justify-around gap-4 ">
                  <p style={{ color: "#8b9aaa" }}>579.514 Thích</p>
                  <div
                    className=" py-2 px-5 rounded font-medium cursor-pointer"
                    style={{ backgroundColor: "#3b5998", color: "#a9b7c4" }}
                  >
                    Chia sẻ
                  </div>
                </div>
              </div>
            </div>
            <div className=" w-max h-max">
              <img
                src="https://www.bhdstar.vn/wp-content/uploads/2018/04/HUE-copy-243x330.jpg"
                alt=""
                className=" w-64 h-96 event_item hover:scale-110"
              />
              <div
                className=" w-full h-24 px-2 py-3"
                style={{
                  backgroundColor: "#172029",
                  border: "3px solid #207f2b",
                }}
              >
                <h2 className=" text-white font-medium cursor-pointer hover:text-yellow-500  transition duration-200">
                  BHD STAR HUẾ
                </h2>
                <div className=" flex mt-4 justify-around gap-4 ">
                  <p style={{ color: "#8b9aaa" }}>233.406 Thích</p>
                  <div
                    className=" py-2 px-5 rounded font-medium cursor-pointer"
                    style={{ backgroundColor: "#3b5998", color: "#a9b7c4" }}
                  >
                    Chia sẻ
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className=" flex justify-evenly mt-10">
            <div className=" w-max h-max">
              <img
                src="https://www.bhdstar.vn/wp-content/uploads/2016/07/PNT-CINEMA-243x330.jpg"
                alt=""
                className=" w-64 h-96 event_item hover:scale-110"
              />
              <div
                className=" w-full h-24 px-2 py-3"
                style={{
                  backgroundColor: "#172029",
                  border: "3px solid #207f2b",
                }}
              >
                <h2 className=" text-white font-medium cursor-pointer hover:text-yellow-500  transition duration-200">
                  BHD STAR PHẠM NGỌC THẠNH
                </h2>
                <div className=" flex mt-4 justify-around gap-4 ">
                  <p style={{ color: "#8b9aaa" }}>1.504.110 Thích</p>
                  <div
                    className=" py-2 px-5 rounded font-medium cursor-pointer"
                    style={{ backgroundColor: "#3b5998", color: "#a9b7c4" }}
                  >
                    Chia sẻ
                  </div>
                </div>
              </div>
            </div>
            <div className=" w-max h-max">
              <img
                src="	https://www.bhdstar.vn/wp-content/uploads/2016/04/LVV-268x364.jpg"
                alt=""
                className=" w-64 h-96 event_item hover:scale-110"
              />
              <div
                className=" w-full h-24 px-2 py-3"
                style={{
                  backgroundColor: "#172029",
                  border: "3px solid #207f2b",
                }}
              >
                <h2 className=" text-white font-medium cursor-pointer hover:text-yellow-500  transition duration-200">
                  BHD STAR LÊ VĂN VIỆT
                </h2>
                <div className=" flex mt-4 justify-around gap-4 ">
                  <p style={{ color: "#8b9aaa" }}>1.042.422 Thích</p>
                  <div
                    className=" py-2 px-5 rounded font-medium cursor-pointer"
                    style={{ backgroundColor: "#3b5998", color: "#a9b7c4" }}
                  >
                    Chia sẻ
                  </div>
                </div>
              </div>
            </div>
            <div className=" w-max h-max">
              <img
                src="https://www.bhdstar.vn/wp-content/uploads/2016/02/THAO-DIEN-CINEMA-268x364.jpg"
                alt=""
                className=" w-64 h-96 event_item hover:scale-110"
              />
              <div
                className=" w-full h-24 px-2 py-3"
                style={{
                  backgroundColor: "#172029",
                  border: "3px solid #207f2b",
                }}
              >
                <h2 className=" text-white font-medium cursor-pointer hover:text-yellow-500  transition duration-200">
                  BHD STAR THẢO ĐIỀN
                </h2>
                <div className=" flex mt-4 justify-around gap-4 ">
                  <p style={{ color: "#8b9aaa" }}>413.633 Thích</p>
                  <div
                    className=" py-2 px-5 rounded font-medium cursor-pointer"
                    style={{ backgroundColor: "#3b5998", color: "#a9b7c4" }}
                  >
                    Chia sẻ
                  </div>
                </div>
              </div>
            </div>
            <div className=" w-max h-max">
              <img
                src="https://www.bhdstar.vn/wp-content/uploads/2015/01/QUANG-TRUNG-243x330.jpg"
                alt=""
                className=" w-64 h-96 event_item hover:scale-110"
              />
              <div
                className=" w-full h-24 px-2 py-3"
                style={{
                  backgroundColor: "#172029",
                  border: "3px solid #207f2b",
                }}
              >
                <h2 className=" text-white font-medium cursor-pointer hover:text-yellow-500  transition duration-200">
                  BHD STAR QUANG TRUNG
                </h2>
                <div className=" flex mt-4 justify-around gap-4 ">
                  <p style={{ color: "#8b9aaa" }}>606.092 Thích</p>
                  <div
                    className=" py-2 px-5 rounded font-medium cursor-pointer"
                    style={{ backgroundColor: "#3b5998", color: "#a9b7c4" }}
                  >
                    Chia sẻ
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className=" flex justify-evenly mt-10">
            <div className=" w-max h-max">
              <img
                src="	https://www.bhdstar.vn/wp-content/uploads/2014/01/PH-268x364.jpg"
                alt=""
                className=" w-64 h-96 event_item hover:scale-110"
              />
              <div
                className=" w-full h-24 px-2 py-3"
                style={{
                  backgroundColor: "#172029",
                  border: "3px solid #207f2b",
                }}
              >
                <h2 className=" text-white font-medium cursor-pointer hover:text-yellow-500  transition duration-200">
                  BHD STAR PHẠM HÙNG
                </h2>
                <div className=" flex mt-4 justify-around gap-4 ">
                  <p style={{ color: "#8b9aaa" }}>600.644 Thích</p>
                  <div
                    className=" py-2 px-5 rounded font-medium cursor-pointer"
                    style={{ backgroundColor: "#3b5998", color: "#a9b7c4" }}
                  >
                    Chia sẻ
                  </div>
                </div>
              </div>
            </div>
            <div className=" w-max h-max">
              <img
                src="https://www.bhdstar.vn/wp-content/uploads/2013/01/Bitexco-cinema-268x364.jpg"
                alt=""
                className=" w-64 h-96 event_item hover:scale-110"
              />
              <div
                className=" w-full h-24 px-2 py-3"
                style={{
                  backgroundColor: "#172029",
                  border: "3px solid #207f2b",
                }}
              >
                <h2 className=" text-white font-medium cursor-pointer hover:text-yellow-500  transition duration-200">
                  BHD STAR BITEXCO
                </h2>
                <div className=" flex mt-4 justify-around gap-4 ">
                  <p style={{ color: "#8b9aaa" }}>475.269 Thích</p>
                  <div
                    className=" py-2 px-5 rounded font-medium cursor-pointer"
                    style={{ backgroundColor: "#3b5998", color: "#a9b7c4" }}
                  >
                    Chia sẻ
                  </div>
                </div>
              </div>
            </div>
            <div className=" w-max h-max">
              <img
                src="https://www.bhdstar.vn/wp-content/uploads/2010/08/3.2-243x330.jpg"
                alt=""
                className=" w-64 h-96 event_item hover:scale-110"
              />
              <div
                className=" w-full h-24 px-2 py-3"
                style={{
                  backgroundColor: "#172029",
                  border: "3px solid #207f2b",
                }}
              >
                <h2 className=" text-white font-medium cursor-pointer hover:text-yellow-500  transition duration-200">
                  BHD STAR 3/2
                </h2>
                <div className=" flex mt-4 justify-around gap-4 ">
                  <p style={{ color: "#8b9aaa" }}>600.369 Thích</p>
                  <div
                    className=" py-2 px-5 rounded font-medium cursor-pointer"
                    style={{ backgroundColor: "#3b5998", color: "#a9b7c4" }}
                  >
                    Chia sẻ
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
