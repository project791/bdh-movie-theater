import React from "react";
import { NavLink } from "react-router-dom";

export default function KhuyenMai() {
  return (
    <div
      className="w-full mx-auto py-20"
      style={{
        backgroundImage:
          "url(https://www.bhdstar.vn/wp-content/themes/bhd/assets/images/bg-cinema-10.png)",
      }}
    >
      <div className="container mx-auto">
        <div className=" px-10">
          <div className=" flex justify-center gap-5">
            <NavLink to={"/khuyenmai"}>
              <h1
                className="text-3xl text-white font-medium text-center mb-10 hover:underline cursor-pointer"
                style={{ color: "#c3cc26" }}
              >
                KHUYẾN MÃI
              </h1>
            </NavLink>
            <span className=" text-3xl text-white">|</span>
            <NavLink to={"/sukien"}>
              <h1 className="text-3xl font-medium text-center mb-10 cursor-pointer text-white hover:underline">
                SỰ KIỆN
              </h1>
            </NavLink>
          </div>
          <div className=" flex justify-evenly">
            <div className=" w-max h-max">
              <img
                src="https://www.bhdstar.vn/wp-content/uploads/2022/07/1920x1080-latdat-270x152.jpg"
                alt=""
                className=" w-64 h-44 event_item hover:scale-110  "
              />
              <div
                className=" w-full h-24 px-2 py-3"
                style={{
                  backgroundColor: "#172029",
                }}
              >
                <h2 className=" text-white font-bold hover:text-yellow-500  transition duration-200 cursor-pointer">
                  NHẬN LẬT ĐẬT XINH
                </h2>
                <div className=" flex mt-4 justify-around gap-4 ">
                  <p style={{ color: "#8b9aaa" }}>1.024 Thích</p>
                  <div
                    className=" py-2 px-5 rounded font-medium cursor-pointer"
                    style={{ backgroundColor: "#3b5998", color: "#a9b7c4" }}
                  >
                    Chia sẻ
                  </div>
                </div>
              </div>
            </div>
            <div className=" w-max h-max">
              <img
                src="https://www.bhdstar.vn/wp-content/uploads/2022/06/1920x1080-PEPSI-270x152.jpg"
                alt=""
                className=" w-64 h-44 event_item hover:scale-110"
              />
              <div
                className=" w-full h-24 px-2 py-3"
                style={{
                  backgroundColor: "#172029",
                }}
              >
                <h2 className=" text-white font-bold hover:text-yellow-500  transition duration-200 cursor-pointer">
                  TƯNG BỪNG HÈ CÙNG PEPSI
                </h2>
                <div className=" flex mt-4 justify-around gap-4 ">
                  <p style={{ color: "#8b9aaa" }}>1.138 Thích</p>
                  <div
                    className=" py-2 px-5 rounded font-medium cursor-pointer"
                    style={{ backgroundColor: "#3b5998", color: "#a9b7c4" }}
                  >
                    Chia sẻ
                  </div>
                </div>
              </div>
            </div>
            <div className=" w-max h-max">
              <img
                src="https://www.bhdstar.vn/wp-content/uploads/2018/03/1920x1080-CTKM-ComboMirinda-270x152.jpg"
                alt=""
                className=" w-64 h-44 event_item hover:scale-110"
              />
              <div
                className=" w-full h-24 px-2 py-3"
                style={{
                  backgroundColor: "#172029",
                }}
              >
                <h2 className=" text-white font-bold hover:text-yellow-500  transition duration-200 cursor-pointer">
                  COMBO MIRINDA SORBET
                </h2>
                <div className=" flex mt-4 justify-around gap-4 ">
                  <p style={{ color: "#8b9aaa" }}>1.280 Thích</p>
                  <div
                    className=" py-2 px-5 rounded font-medium cursor-pointer"
                    style={{ backgroundColor: "#3b5998", color: "#a9b7c4" }}
                  >
                    Chia sẻ
                  </div>
                </div>
              </div>
            </div>
            <div className=" w-max h-max">
              <img
                src="https://www.bhdstar.vn/wp-content/uploads/2018/03/Package-U22-270x152.png"
                alt=""
                className=" w-64 h-44 event_item hover:scale-110"
              />
              <div
                className=" w-full h-24 px-2 py-3"
                style={{
                  backgroundColor: "#172029",
                }}
              >
                <h2 className=" text-white font-bold hover:text-yellow-500  transition duration-200 cursor-pointer">
                  GIÁ RẺ CHO GIỚI TRẺ
                </h2>
                <div className=" flex mt-4 justify-around gap-4 ">
                  <p style={{ color: "#8b9aaa" }}>32.701 Thích</p>
                  <div
                    className=" py-2 px-5 rounded font-medium cursor-pointer"
                    style={{ backgroundColor: "#3b5998", color: "#a9b7c4" }}
                  >
                    Chia sẻ
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className=" flex justify-evenly mt-10">
            <div className=" w-max h-max">
              <img
                src="https://www.bhdstar.vn/wp-content/uploads/2018/03/U22-web-1-270x152.png"
                alt=""
                className=" w-64 h-44 event_item hover:scale-110"
              />
              <div
                className=" w-full h-24 px-2 py-3"
                style={{
                  backgroundColor: "#172029",
                }}
              >
                <h2 className=" text-white font-bold hover:text-yellow-500  transition duration-200 cursor-pointer">
                  ƯU ĐÃI ĐẶC BIỆT CHO U22
                </h2>
                <div className=" flex mt-4 justify-around gap-4 ">
                  <p style={{ color: "#8b9aaa" }}>430.840 Thích</p>
                  <div
                    className=" py-2 px-5 rounded font-medium cursor-pointer"
                    style={{ backgroundColor: "#3b5998", color: "#a9b7c4" }}
                  >
                    Chia sẻ
                  </div>
                </div>
              </div>
            </div>
            <div className=" w-max h-max">
              <img
                src="https://www.bhdstar.vn/wp-content/uploads/2018/03/Web-HappyDay-270x152.png"
                alt=""
                className=" w-64 h-44 event_item hover:scale-110"
              />
              <div
                className=" w-full h-24 px-2 py-3"
                style={{
                  backgroundColor: "#172029",
                }}
              >
                <h2 className=" text-white font-bold hover:text-yellow-500  transition duration-200 cursor-pointer">
                  HAPPY MONDAY THỨ 2 VUI VẺ
                </h2>
                <div className=" flex mt-4 justify-around gap-4 ">
                  <p style={{ color: "#8b9aaa" }}>147.519 Thích</p>
                  <div
                    className=" py-2 px-5 rounded font-medium cursor-pointer"
                    style={{ backgroundColor: "#3b5998", color: "#a9b7c4" }}
                  >
                    Chia sẻ
                  </div>
                </div>
              </div>
            </div>
            <div className=" w-max h-max">
              <img
                src="https://www.bhdstar.vn/wp-content/uploads/2018/03/Suat-Khuya-Web-270x152.jpg"
                alt=""
                className=" w-64 h-44 event_item hover:scale-110"
              />
              <div
                className=" w-full h-24 px-2 py-3"
                style={{
                  backgroundColor: "#172029",
                }}
              >
                <h2 className=" text-white font-bold hover:text-yellow-500  transition duration-200 cursor-pointer">
                  GIÁ VÉ ƯU ĐÃI CHO XUẤT KHUYA
                </h2>
                <div className=" flex mt-4 justify-around gap-4 ">
                  <p style={{ color: "#8b9aaa" }}>53.107 Thích</p>
                  <div
                    className=" py-2 px-5 rounded font-medium cursor-pointer"
                    style={{ backgroundColor: "#3b5998", color: "#a9b7c4" }}
                  >
                    Chia sẻ
                  </div>
                </div>
              </div>
            </div>
            <div className=" w-max h-max">
              <img
                src="    https://www.bhdstar.vn/wp-content/uploads/2018/04/BHD-Star-HUE-KhaiTruong-Teasing-315x420.jpg"
                alt=""
                className=" w-64 h-44 event_item hover:scale-110"
              />
              <div
                className=" w-full h-24 px-2 py-3"
                style={{
                  backgroundColor: "#172029",
                }}
              >
                <h2 className=" text-white font-bold hover:text-yellow-500  transition duration-200 cursor-pointer">
                  BHD STAR HUẾ
                </h2>
                <div className=" flex mt-4 justify-around gap-4 ">
                  <p style={{ color: "#8b9aaa" }}>33.634 Thích</p>
                  <div
                    className=" py-2 px-5 rounded font-medium cursor-pointer"
                    style={{ backgroundColor: "#3b5998", color: "#a9b7c4" }}
                  >
                    Chia sẻ
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
