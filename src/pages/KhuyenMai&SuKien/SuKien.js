import React from "react";
import { NavLink } from "react-router-dom";
export default function SuKien() {
  return (
    <div
      className="w-full mx-auto py-20"
      style={{
        backgroundImage:
          "url(https://www.bhdstar.vn/wp-content/themes/bhd/assets/images/bg-cinema-10.png)",
      }}
    >
      <div className="container mx-auto">
        <div className=" px-10">
          <div className=" flex justify-center gap-5">
            <NavLink to={"/khuyenmai"}>
              <h1 className="text-3xl text-white font-medium text-center mb-10 hover:underline cursor-pointer">
                KHUYẾN MÃI
              </h1>
            </NavLink>
            <span className=" text-3xl text-white">|</span>
            <NavLink to={"/sukien"}>
              <h1
                className="text-3xl font-medium text-center mb-10 cursor-pointer hover:underline"
                style={{ color: "#c3cc26" }}
              >
                SỰ KIỆN
              </h1>
            </NavLink>
          </div>
          <div className=" flex justify-evenly">
            <div className=" w-max h-max">
              <img
                src="https://www.bhdstar.vn/wp-content/uploads/2019/12/Banner-billboard-3m2x4m3-268x360.jpg    "
                alt=""
                className=" w-64 h-96 event_item hover:scale-110  "
              />
              <div
                className=" w-full h-24 px-2 py-3"
                style={{
                  backgroundColor: "#172029",
                }}
              >
                <h2 className=" text-white font-bold hover:text-yellow-500  transition duration-200 cursor-pointer">
                  BHD STAR GARDEN
                </h2>
                <div className=" flex mt-4 justify-around gap-4 ">
                  <p style={{ color: "#8b9aaa" }}>21.100 Thích</p>
                  <div
                    className=" py-2 px-5 rounded font-medium cursor-pointer"
                    style={{ backgroundColor: "#3b5998", color: "#a9b7c4" }}
                  >
                    Chia sẻ
                  </div>
                </div>
              </div>
            </div>
            <div className=" w-max h-max">
              <img
                src="
               https://www.bhdstar.vn/wp-content/uploads/2019/08/BHD-Star-ScanQR-315x420.jpg"
                alt=""
                className=" w-64 h-96 event_item hover:scale-110"
              />
              <div
                className=" w-full h-24 px-2 py-3"
                style={{
                  backgroundColor: "#172029",
                }}
              >
                <h2 className=" text-white font-bold hover:text-yellow-500  transition duration-200 cursor-pointer">
                  QUÉT MÃ QR NHANH VÀO RẠP!
                </h2>
                <div className=" flex mt-4 justify-around gap-4 ">
                  <p style={{ color: "#8b9aaa" }}>97.559 Thích</p>
                  <div
                    className=" py-2 px-5 rounded font-medium cursor-pointer"
                    style={{ backgroundColor: "#3b5998", color: "#a9b7c4" }}
                  >
                    Chia sẻ
                  </div>
                </div>
              </div>
            </div>

            <div className=" w-max h-max">
              <img
                src="https://www.bhdstar.vn/wp-content/uploads/2019/03/A4appmoi-268x379.jpg"
                alt=""
                className=" w-64 h-96 event_item hover:scale-110"
              />
              <div
                className=" w-full h-24 px-2 py-3"
                style={{
                  backgroundColor: "#172029",
                }}
              >
                <h2 className=" text-white font-bold hover:text-yellow-500  transition duration-200 cursor-pointer">
                  ỨNG DỤNG MUA VÉ MỚI
                </h2>
                <div className=" flex mt-4 justify-around gap-4 ">
                  <p style={{ color: "#8b9aaa" }}>10.701 Thích</p>
                  <div
                    className=" py-2 px-5 rounded font-medium cursor-pointer"
                    style={{ backgroundColor: "#3b5998", color: "#a9b7c4" }}
                  >
                    Chia sẻ
                  </div>
                </div>
              </div>
            </div>
            <div className=" w-max h-max">
              <img
                src="https://www.bhdstar.vn/wp-content/uploads/2018/08/BHD-STAR-CHECK-IN-CUNG-GAU-314X420-268x357.jpg"
                alt=""
                className=" w-64 h-96 event_item hover:scale-110"
              />
              <div
                className=" w-full h-24 px-2 py-3"
                style={{
                  backgroundColor: "#172029",
                }}
              >
                <h2 className=" text-white font-bold hover:text-yellow-500  transition duration-200 cursor-pointer">
                  ĐIỂM HẸN MỚI CỦA GIỚI TRẺ
                </h2>
                <div className=" flex mt-4 justify-around gap-4 ">
                  <p style={{ color: "#8b9aaa" }}>1.701 Thích</p>
                  <div
                    className=" py-2 px-5 rounded font-medium cursor-pointer"
                    style={{ backgroundColor: "#3b5998", color: "#a9b7c4" }}
                  >
                    Chia sẻ
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className=" flex justify-evenly mt-10">
            <div className=" w-max h-max">
              <img
                src="https://www.bhdstar.vn/wp-content/uploads/2017/10/BHD-Star_Hotline_315x420-2.png"
                alt=""
                className=" w-64 h-96 event_item hover:scale-110"
              />
              <div
                className=" w-full h-24 px-2 py-3"
                style={{
                  backgroundColor: "#172029",
                }}
              >
                <h2 className=" text-white font-bold hover:text-yellow-500  transition duration-200 cursor-pointer">
                  ĐƯỜNG DÂY NÓNG BHD STAR
                </h2>
                <div className=" flex mt-4 justify-around gap-4 ">
                  <p style={{ color: "#8b9aaa" }}>9.787 Thích</p>
                  <div
                    className=" py-2 px-5 rounded font-medium cursor-pointer"
                    style={{ backgroundColor: "#3b5998", color: "#a9b7c4" }}
                  >
                    Chia sẻ
                  </div>
                </div>
              </div>
            </div>
            <div className=" w-max h-max">
              <img
                src="
                  https://www.bhdstar.vn/wp-content/uploads/2016/11/BHDSTAR-HA-NOI-KHAI-TRUONG.png"
                alt=""
                className=" w-64 h-96 event_item hover:scale-110"
              />
              <div
                className=" w-full h-24 px-2 py-3"
                style={{
                  backgroundColor: "#172029",
                }}
              >
                <h2 className=" text-white font-bold hover:text-yellow-500  transition duration-200 cursor-pointer">
                  BHD STAR PHẠM NGỌC THẠCH
                </h2>
                <div className=" flex mt-4 justify-around gap-4 ">
                  <p style={{ color: "#8b9aaa" }}>22.039 Thích</p>
                  <div
                    className=" py-2 px-5 rounded font-medium cursor-pointer"
                    style={{ backgroundColor: "#3b5998", color: "#a9b7c4" }}
                  >
                    Chia sẻ
                  </div>
                </div>
              </div>
            </div>
            <div className=" w-max h-max">
              <img
                src="    https://www.bhdstar.vn/wp-content/uploads/2018/07/BHD-Star-HuongDanOnline-315x420.jpg"
                alt=""
                className=" w-64 h-96 event_item hover:scale-110"
              />
              <div
                className=" w-full h-24 px-2 py-3"
                style={{
                  backgroundColor: "#172029",
                }}
              >
                <h2 className=" text-white font-bold hover:text-yellow-500  transition duration-200 cursor-pointer">
                  THẺ THÀNH VIÊN ĐIỆN TỬ
                </h2>
                <div className=" flex mt-4 justify-around gap-4 ">
                  <p style={{ color: "#8b9aaa" }}>32.858 Thích</p>
                  <div
                    className=" py-2 px-5 rounded font-medium cursor-pointer"
                    style={{ backgroundColor: "#3b5998", color: "#a9b7c4" }}
                  >
                    Chia sẻ
                  </div>
                </div>
              </div>
            </div>
            <div className=" w-max h-max">
              <img
                src="    https://www.bhdstar.vn/wp-content/uploads/2018/04/BHD-Star-HUE-KhaiTruong-Teasing-315x420.jpg"
                alt=""
                className=" w-64 h-96 event_item hover:scale-110"
              />
              <div
                className=" w-full h-24 px-2 py-3"
                style={{
                  backgroundColor: "#172029",
                }}
              >
                <h2 className=" text-white font-bold hover:text-yellow-500  transition duration-200 cursor-pointer">
                  BHD STAR HUẾ
                </h2>
                <div className=" flex mt-4 justify-around gap-4 ">
                  <p style={{ color: "#8b9aaa" }}>33.634 Thích</p>
                  <div
                    className=" py-2 px-5 rounded font-medium cursor-pointer"
                    style={{ backgroundColor: "#3b5998", color: "#a9b7c4" }}
                  >
                    Chia sẻ
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
