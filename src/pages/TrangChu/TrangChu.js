import React, { useEffect, useState } from "react";
import { movieService } from "../../services/MovieService";
import { Carousel } from "antd";
import _ from "lodash";
import { BackTop } from "antd";
import DanhSachPhim from "./DanhSachPhim/DanhSachPhim";
export default function TrangChu() {
  const [Banner, setBanner] = useState([]);
  const [MovieList, setMovieList] = useState([]);
  useEffect(() => {
    movieService
      .getBannerMovie()
      .then((res) => {
        setBanner(res.data.content);
      })
      .catch((err) => {});
    movieService
      .getMovieList()
      .then((res) => {
        let chunkList = _.chunk(res.data.content, 6);
        setMovieList(chunkList);
      })
      .catch((err) => {});
  }, []);
  return (
    <div className="mx-auto w-full">
      <div className="banner_content">
        <Carousel autoplay dots={true}>
          {Banner.map((item) => {
            return (
              <div key={item.maPhim}>
                <img
                  style={{
                    minHeight: "860px",
                    objectFit: "cover",
                  }}
                  src={item.hinhAnh}
                  alt=""
                />
              </div>
            );
          })}
        </Carousel>
        <div name="muaVe">
          <DanhSachPhim MovieList={MovieList} />
        </div>
      </div>
      <BackTop>
        <div className=" w-36 flex">
          <span className="text_backToTop">Lên đầu trang</span>
          <div className=" text-xxl text-white backToTop">V</div>
        </div>
      </BackTop>
    </div>
  );
}
