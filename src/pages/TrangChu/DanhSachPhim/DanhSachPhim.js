import { Carousel } from "antd";
import React from "react";
import PhimItem from "./PhimItem";
export default function DanhSachPhim({ MovieList }) {
  const onChange = (currentSlide) => {};
  return (
    <div className="carousel_content">
      <Carousel afterChange={onChange}>
        {MovieList.map((movie, index) => {
          return (
            <div key={index} className=" h-max w-full  py-20">
              <div
                className="grid grid-cols-6 gap-10"
                style={{ height: "430px" }}
              >
                {movie.map((item) => {
                  return <PhimItem item={item} />;
                })}
              </div>
            </div>
          );
        })}
      </Carousel>
    </div>
  );
}
