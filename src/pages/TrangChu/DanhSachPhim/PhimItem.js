import React from "react";
import { NavLink } from "react-router-dom";

export default function PhimItem({ item }) {
  return (
    <div className=" relative" key={item.maPhim}>
      <NavLink to={`/chitiet/${item.maPhim}`}>
        <div className=" overflow-hidden">
          <img
            src={item.hinhAnh}
            alt=""
            className="image_film rounded  hover:scale-110 w-full"
          />
        </div>
        <h3 className=" text-white font-medium text-center text-lg uppercase mt-3">
          {item.tenPhim}
        </h3>
        <div
          className=" absolute w-full bottom-0  py-2 rounded text-white text-center font-bold mt-3 btn_muaVe"
          style={{ backgroundColor: "#54ab35" }}
        >
          MUA VÉ
        </div>
      </NavLink>
    </div>
  );
}
