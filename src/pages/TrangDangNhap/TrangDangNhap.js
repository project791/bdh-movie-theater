import { Button, Form, Input, message } from "antd";
import { useDispatch } from "react-redux";
import { NavLink, useHistory } from "react-router-dom";
import { setUserInforAction } from "../../REDUX/action/action";
import { localStorageService } from "../../services/LocalStorage";
import { userService } from "../../services/UserService";
const TrangDangNhap = () => {
  let dispatch = useDispatch();
  let history = useHistory();
  const onFinish = (values) => {
    userService
      .postDangNhap(values)
      .then((res) => {
        localStorageService.setUserInfor(res.data.content);
        dispatch(setUserInforAction(res.data.content));
        message.success("Đăng nhập thành công");
        setTimeout(() => {
          history.push("/");
        }, 2000);
      })
      .catch((err) => {
        message.error("Sai tài khoản hoặc mật khẩu");
      });
  };
  const onFinishFailed = (errorInfo) => {};
  return (
    <div className="form_content">
      <div className="form_login">
        <Form
          name="basic"
          layout="vertical"
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          initialValues={{
            remember: true,
          }}
          autoComplete="off"
        >
          <Form.Item
            label={<p className="text-white">Tài khoản</p>}
            name="taiKhoan"
            rules={[
              {
                required: true,
                message: "Please input your username!",
              },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            label={<p className="text-white">Mật khẩu</p>}
            name="matKhau"
            rules={[
              {
                required: true,
                message: "Please input your password!",
              },
            ]}
          >
            <Input.Password />
          </Form.Item>
          <div className=" pb-5 flex">
            <Button className="btn_dangNhap" htmlType="submit">
              <h3 className=" text-white">Đăng nhập</h3>
            </Button>
            <NavLink to={"/dangky"}>
              <p className="ml-6 text-white underline">Đăng ký tài khoản</p>
            </NavLink>
          </div>
        </Form>
      </div>
    </div>
  );
};
export default TrangDangNhap;
