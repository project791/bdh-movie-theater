import { Tabs } from "antd";
import MovieTabItem from "./MovieTabItem";
const { TabPane } = Tabs;

export default function DanhSachLichChieuTheoPhim({ detailMovie }) {
  let renderContent = () => {
    return detailMovie.map((heThongRap, index) => {
      return (
        <TabPane
          tab={<img src={heThongRap.logo} className=" w-20 h-20" />}
          key={index}
        >
          <Tabs tabPosition="left" defaultActiveKey="1">
            {heThongRap.cumRapChieu.map((cumRap, index) => {
              return (
                <TabPane
                  tab={
                    <div
                      className=" w-96 whitespace-normal text-left text-white font-medium py-3 px-3 rounded"
                      style={{ backgroundColor: "#1c2936", color: "#a6b2c9" }}
                    >
                      <p className=" text-white text-xl mb-3">
                        {cumRap.tenCumRap}
                      </p>
                      <p>{cumRap.diaChi}</p>
                    </div>
                  }
                  key={index}
                >
                  <div
                    className=" grid-cols-3 grid"
                    style={{ height: 500, overflowY: "scroll" }}
                  >
                    {cumRap.lichChieuPhim.map((item, index) => {
                      return <MovieTabItem movie={item} key={index} />;
                    })}
                  </div>
                </TabPane>
              );
            })}
          </Tabs>
        </TabPane>
      );
    });
  };
  return (
    <Tabs tabPosition="left" defaultActiveKey="1">
      {renderContent()}
    </Tabs>
  );
}
