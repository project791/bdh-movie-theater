import moment from "moment";
import React from "react";
import { NavLink } from "react-router-dom";

export default function MovieTabItem({ movie }) {
  return (
    <div className=" py-5 text-white">
      <NavLink to={`/datVe/${movie.maLichChieu}`}>
        <div className=" w-max rounded font-medium py-3 px-3 cursor-pointer lichChieu">
          <div className="flex space-x-5 ">
            <p>{movie.tenRap}</p>

            <p>{moment(movie.ngayChieuGioChieu).format("DD/MM/YYYY")}</p>
          </div>
          <p>Thời lượng phim : {movie.thoiLuong} phút</p>
        </div>
      </NavLink>
    </div>
  );
}
