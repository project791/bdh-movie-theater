import { Progress } from "antd";
import moment from "moment";
import React, { useEffect, useState } from "react";
import ModalVideo from "react-modal-video";
import { useSelector, useDispatch } from "react-redux";
import { NavLink, useParams } from "react-router-dom";
import { Link, Element } from "react-scroll";
import {
  handleCloseTrailer,
  handleOpenTrailer,
} from "../../REDUX/action/action";
import { movieService } from "../../services/MovieService";
import DanhSachLichChieuTheoPhim from "./DanhSachLichChieuTheoPhim/DanhSachLichChieuTheoPhim";
export default function ChiTietPhim() {
  let trailer = useSelector((state) => state.userReducer.trailer);
  let dispatch = useDispatch();
  let { maPhim } = useParams();
  const [movie, setMovie] = useState({});
  const [detailMovie, setDetailMovie] = useState([]);
  useEffect(() => {
    movieService
      .getDetailMovie(maPhim)
      .then((res) => {
        setMovie(res.data.content);
      })
      .catch((err) => {});
    movieService
      .getThongTinLichChieu(maPhim)
      .then((res) => {
        setDetailMovie(res.data.content.heThongRapChieu);
      })
      .catch((err) => {});
    window.scrollTo(0, 0);
  }, []);
  return (
    <div className="detail_container  mx-auto w-full">
      <div className="detail_content">
        <div className=" text-3xl text-white mb-10 ">
          <NavLink to={"/"}>
            <span className="detail_text">Trang chủ</span>
          </NavLink>
          <span className="mx-3">|</span>
          <span className="uppercase">{movie.tenPhim}</span>
        </div>
        <div className="flex">
          <img
            src={movie.hinhAnh}
            alt=""
            className="rounded cursor-pointer image_item"
          />
          <div className="detail_item ml-10">
            <h3
              className=" text-3xl text-white mb-5 font-bold uppercase"
              style={{ color: "#54ab34" }}
            >
              {movie.tenPhim}
            </h3>
            <h4 className="mb-5 font-medium" style={{ color: "#8f97b3" }}>
              {movie.moTa}
            </h4>
            <h4 className="text-white font-medium mt-5 mb-5">
              Ngày khởi chiếu {moment(movie.ngayKhoiChieu).format("DD/MM/YYYY")}
            </h4>
            <div className="flex">
              <h4 className=" text-white font-medium">Điểm đánh giá : </h4>
              <Progress
                type="circle"
                percent={movie.danhGia * 10}
                width={150}
                strokeColor={{
                  "0%": "#108ee9",
                  "100%": "#87d068",
                }}
                strokeWidth={10}
                format={(number) => {
                  return (
                    <span className="text-blue-700"> {number / 10} điểm</span>
                  );
                }}
              />
            </div>

            <div className="flex">
              <div>
                <React.Fragment>
                  <ModalVideo
                    channel="youtube"
                    autoplay
                    isOpen={trailer.isOpen}
                    videoId={trailer.id}
                    onClose={() => {
                      dispatch(handleCloseTrailer());
                    }}
                  />
                  <div
                    className=" btn_item text-white font-medium mt-3 py-3 font-bold"
                    onClick={() => dispatch(handleOpenTrailer(movie.trailer))}
                  >
                    TRAILER
                  </div>
                </React.Fragment>
              </div>
              <Link
                to="test1"
                spy={true}
                smooth={true}
                offset={-100}
                duration={500}
              >
                <div className=" btn_item text-white font-bold mt-3 py-3">
                  MUA VÉ
                </div>
              </Link>
            </div>
          </div>
        </div>
      </div>
      <div name="test1" className="detail_content">
        <DanhSachLichChieuTheoPhim detailMovie={detailMovie} />
      </div>
    </div>
  );
}
