import { Button, Form, Input, message } from "antd";
import { useHistory } from "react-router-dom";
import { userService } from "../../services/UserService";

const TrangDangKy = () => {
  let history = useHistory();
  const onFinish = (values) => {
    userService
      .postDangKy(values)
      .then((res) => {
        message.success("Đăng ký thành công");
        setTimeout(() => {
          history.push("/dangnhap");
        }, 2000);
      })
      .catch((err) => {
        message.error(err.response.data.message);
      });
  };
  const onFinishFailed = (errorInfo) => {};
  return (
    <div className="form_content">
      <div className="form_login">
        <Form
          name="basic"
          layout="vertical"
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          initialValues={{
            remember: true,
          }}
          autoComplete="off"
        >
          <Form.Item
            label={<p className="text-white">Họ tên</p>}
            name="hoTen"
            rules={[
              {
                required: true,
                message: "Vui lòng nhập họ tên",
                pattern: new RegExp(
                  /^[a-zA-ZÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂẾưăạảấầẩẫậắằẳẵặẹẻẽềềểếỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\s\W|_]+$/
                ),
              },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            label={<p className="text-white">Tài khoản</p>}
            name="taiKhoan"
            rules={[
              {
                required: true,
                message: "Vui lòng nhập tài khoản",
                pattern: new RegExp(
                  /^(((?=.*[a-z])(?=.*[A-Z]))|((?=.*[a-z])(?=.*[0-9]))|((?=.*[A-Z])(?=.*[0-9])))(?=.{6,})/
                ),
              },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            label={<p className="text-white">Mật khẩu</p>}
            name="matKhau"
            rules={[
              {
                required: true,
                message: "Vui lòng nhập mật khẩu",
                pattern: new RegExp(
                  /^(((?=.*[a-z])(?=.*[A-Z]))|((?=.*[a-z])(?=.*[0-9]))|((?=.*[A-Z])(?=.*[0-9])))(?=.{6,})/
                ),
              },
            ]}
          >
            <Input.Password />
          </Form.Item>
          <Form.Item
            label={<p className="text-white">Email</p>}
            name="email"
            rules={[
              {
                required: true,
                message: "Vui lòng nhập đúng email",
                pattern: new RegExp(
                  /^[a-z0-9](\.?[a-z0-9]){5,}@g(oogle)?mail\.com$/
                ),
              },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            label={<p className="text-white">Số điện thoại</p>}
            name="soDt"
            rules={[
              {
                required: true,
                message: "Vui lòng nhập đúng số điện thoại",
                pattern: new RegExp(/^[0-9]*$/),
              },
            ]}
          >
            <Input />
          </Form.Item>
          <div className=" pb-5">
            <Button className="btn_dangNhap" htmlType="submit">
              <h3 className="text-white">ĐĂNG KÝ</h3>
            </Button>
          </div>
        </Form>
      </div>
    </div>
  );
};
export default TrangDangKy;
