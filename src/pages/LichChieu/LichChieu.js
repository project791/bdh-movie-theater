import React, { useEffect, useState } from "react";
import { getMovieListActionService } from "../../REDUX/action/action";
import _ from "lodash";
import { Carousel } from "antd";
import { NavLink } from "react-router-dom";
export default function LichChieu() {
  const [Movie, setMovie] = useState([]);
  useEffect(() => {
    getMovieListActionService()
      .then((res) => {
        let chunkList = _.chunk(res.data.content, 6);
        setMovie(chunkList);
      })
      .catch((err) => {});
  }, []);
  const onChange = (currentSlide) => {};
  return (
    <div className="carousel_content">
      <h2
        style={{ color: "#c3cc26" }}
        className="text-center text-white font-medium text-3xl pt-20 underline"
      >
        Lịch chiếu theo phim
      </h2>
      <Carousel afterChange={onChange}>
        {Movie.map((movie, index) => {
          return (
            <div className=" h-max w-full  py-20">
              <div className="grid grid-cols-6 gap-10">
                {movie.map((item) => {
                  return (
                    <NavLink to={`/chitiet/${item.maPhim}`}>
                      <div>
                        <div className=" overflow-hidden">
                          <img
                            src={item.hinhAnh}
                            alt=""
                            className="image_film rounded  hover:scale-110 w-full"
                          />
                        </div>
                        <h3 className=" text-white font-medium text-center text-lg uppercase mt-3">
                          {item.tenPhim}
                        </h3>
                      </div>
                    </NavLink>
                  );
                })}
              </div>
            </div>
          );
        })}
      </Carousel>
    </div>
  );
}
