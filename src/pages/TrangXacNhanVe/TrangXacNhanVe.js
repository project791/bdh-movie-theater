import React, { useEffect } from "react";
import { useSelector } from "react-redux";

export default function TrangXacNhanVe() {
  const vnd = (number) => {
    return (number * 1)
      .toLocaleString("it-IT", {
        style: "currency",
        currency: "VND",
      })
      .replace("VND", "đ");
  };
  let selectedSeat = useSelector((state) => {
    return state.userReducer.selectedSeat;
  });
  let userInfor = useSelector((state) => {
    return state.userReducer.userInfor;
  });
  let movieInfor = useSelector((state) => {
    return state.userReducer.confirmTicket;
  });
  let handleGoBackTrangChu = () => {
    window.location.href = "/";
  };
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  return (
    <div
      style={{
        backgroundImage:
          "url(https://www.bhdstar.vn/wp-content/themes/bhd/assets/images/movie-details-bg.jpg)",
      }}
      className="w-full py-16"
    >
      <div className="container mx-auto">
        <div className=" form_datVe px-8 py-5 w-96 mx-auto">
          <div className="w-full">
            <div className="mb-5">
              <h2 className="text-white font-medium text-xl text-center">
                THÔNG TIN VÉ CỦA BẠN
              </h2>
            </div>
            <div className=" w-full">
              <div className="grid grid-cols-2">
                <p className=" text-white text-xl">HỌ TÊN :</p>
                <p
                  className="  font-medium text-xl"
                  style={{ color: "#54ab34" }}
                >
                  {userInfor.hoTen}
                </p>
              </div>
              <div className="grid grid-cols-2 my-5">
                <p className=" text-white text-xl">TÊN PHIM :</p>
                <p
                  className=" uppercase font-medium text-xl"
                  style={{ color: "#54ab34" }}
                >
                  {movieInfor?.thongTinPhim?.tenPhim}
                </p>
              </div>
              <div className="grid grid-cols-2 my-5">
                <p className=" text-white text-xl">RẠP:</p>
                <p
                  className=" uppercase font-medium text-xl"
                  style={{ color: "#54ab34" }}
                >
                  {movieInfor?.thongTinPhim?.tenRap}
                </p>
              </div>
              <div className="grid grid-cols-2 my-5">
                <p className=" text-white text-xl">NGÀY CHIẾU:</p>
                <p
                  className=" uppercase font-medium text-xl"
                  style={{ color: "#54ab34" }}
                >
                  {movieInfor?.thongTinPhim?.ngayChieu}
                </p>
              </div>
              <div className="grid grid-cols-2 my-5">
                <p className=" text-white text-xl">GIỜ CHIẾU:</p>
                <p
                  className=" uppercase font-medium text-xl"
                  style={{ color: "#54ab34" }}
                >
                  {movieInfor?.thongTinPhim?.gioChieu}
                </p>
              </div>
              <div className="grid grid-cols-2">
                <p className=" text-white text-xl">SỐ GHẾ BẠN CHỌN :</p>
                <div>
                  <p
                    className=" break-words font-medium text-xl"
                    style={{ color: "#54ab34" }}
                  >
                    {selectedSeat
                      ?.map((item) => {
                        return `${item.stt}`;
                      })
                      .join(",")}
                  </p>
                </div>
              </div>
              <div className="grid grid-cols-2 my-5">
                <p className=" text-white text-xl">TỔNG TIỀN :</p>
                <p
                  className=" uppercase font-medium text-xl"
                  style={{ color: "#54ab34" }}
                >
                  {vnd(
                    selectedSeat?.reduce((total, seat) => {
                      return total + seat.giaVe;
                    }, 0)
                  )}
                </p>
              </div>
            </div>
            <div className=" flex justify-end gap-2"></div>
          </div>
          <div
            className="text-white font-medium text-lg text-center cursor-pointer "
            style={{ backgroundColor: "#54ab34" }}
            onClick={handleGoBackTrangChu}
          >
            VỀ TRANG CHỦ
          </div>
        </div>
      </div>
    </div>
  );
}
