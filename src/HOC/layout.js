import React from "react";
import FooterTheme from "../Components/FooterTheme/FooterTheme";
import HeaderTheme from "../Components/HeaderTheme/HeaderTheme";

export function LayoutTheme({ Component }) {
  return (
    <div>
      <HeaderTheme />
      <Component />
      <FooterTheme />
    </div>
  );
}
