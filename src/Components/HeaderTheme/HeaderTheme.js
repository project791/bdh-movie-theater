import React from "react";
import { NavLink } from "react-router-dom";
import DropdownMenu from "./DropdownMenu";
import "./header.css";
import UserNav from "./UserNav";
import { Link } from "react-scroll";

function HeaderTheme() {
  return (
    <header>
      <div className="main_header">
        <div className="flex main_container">
          <DropdownMenu />
          <Link
            to="muaVe"
            spy={true}
            smooth={true}
            offset={-100}
            duration={500}
          >
            <div className="header_buyticket">
              <h3 className="  font-bold text-center mt-2 text-white">
                MUA VÉ
              </h3>
            </div>
          </Link>
          <div>
            <NavLink to={"/"}>
              <img
                src="https://www.bhdstar.vn/wp-content/themes/bhd/assets/images/logo.png"
                alt=""
                className="logo_header"
              />
            </NavLink>
          </div>
          <div className="flex social_app">
            <a
              href="https://www.instagram.com/bhdstar.cineplex/"
              target="_blank"
            >
              <img
                src="https://www.bhdstar.vn/wp-content/themes/bhd/assets/images/icon_in.png"
                alt=""
                className="logo_social"
              />
            </a>
            <a href="https://www.tiktok.com/@bhdstar.cineplex" target="_blank">
              <img
                src="https://www.bhdstar.vn/wp-content/themes/bhd/assets/images/icon_tiktok.png"
                className="logo_social"
                alt=""
              />
            </a>
            <a href="https://www.youtube.com/user/BHDStar" target="_blank">
              <img
                src="https://www.bhdstar.vn/wp-content/themes/bhd/assets/images/icon_YT.png"
                className="logo_social"
                alt=""
              />
            </a>
            <a href="https://www.facebook.com/BHDStar" target="_blank">
              <img
                src="https://www.bhdstar.vn/wp-content/themes/bhd/assets/images/icon_fb.png"
                className="logo_social"
                alt=""
              />
            </a>
          </div>
          <UserNav />
        </div>
      </div>
      <img
        src="https://www.bhdstar.vn/wp-content/themes/bhd/assets/images/line-header1.png"
        alt=""
        className="header_overlay"
      />
    </header>
  );
}
export default HeaderTheme;
