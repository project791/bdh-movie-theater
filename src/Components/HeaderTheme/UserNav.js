import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";

import { NavLink } from "react-router-dom";
import { localStorageService } from "../../services/LocalStorage";

export default function UserNav() {
  let userInfor = useSelector((state) => {
    return state.userReducer.userInfor;
  });
  let dispatch = useDispatch();
  let handleLogout = () => {
    localStorageService.removeUserInfor();
    window.location.href = "/dangnhap";
  };
  return (
    <div>
      <div>
        {userInfor ? (
          <div>
            <div>
              <p className="username">Tài khoản : {userInfor?.hoTen}</p>
            </div>
            <div onClick={handleLogout} className="header_login">
              <h3 className="font-bold text-center  text-white">ĐĂNG XUẤT</h3>
            </div>
          </div>
        ) : (
          <NavLink to={"/dangnhap"}>
            <div onClick={handleLogout} className="header_login">
              <p className=" font-bold">ĐĂNG NHẬP</p>
            </div>
          </NavLink>
        )}
      </div>
    </div>
  );
}
