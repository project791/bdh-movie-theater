import React, { useState } from "react";
import { NavLink } from "react-router-dom";

export default function DropdownMenu() {
  const [isActive, setIsActive] = useState(false);
  return (
    <div
      className="dropdown"
      onClick={(e) => {
        setIsActive(!isActive);
      }}
    >
      <div className="header_menu"></div>
      <h3 className=" text-white font-medium text-center mt-8 dropdown_btn">
        MENU
      </h3>
      {isActive && (
        <div className="dropdown_content">
          <div className="dropdown_item">
            <NavLink to={"/lichchieu"}>
              <h3>LỊCH CHIẾU</h3>
            </NavLink>
          </div>
          <div className="dropdown_item">
            <NavLink to={"/hethongrap"}>
              <h3>HỆ THỐNG RẠP</h3>
            </NavLink>
          </div>
          <div className="dropdown_item">
            <NavLink to={"/khuyenmai"}>
              <h3>KHUYẾN MÃI | SỰ KIỆN</h3>
            </NavLink>
          </div>
          <div className="dropdown_item">
            <NavLink to={"/dichvu"}>
              <h3>DỊCH VỤ QUẢNG CÁO</h3>
            </NavLink>
          </div>
          <div className="dropdown_item">
            <NavLink to={"/tuyendung"}>
              <h3>TUYỂN DỤNG</h3>
            </NavLink>
          </div>
          <div className="dropdown_item">
            <NavLink to={"/vechungtoi"}>
              <h3>VỀ CHÚNG TÔI</h3>
            </NavLink>
          </div>
        </div>
      )}
    </div>
  );
}
