import React from "react";
import "./footer.css";
export default function FooterTheme() {
  return (
    <footer id="footer">
      <div className="main_containerFooter">
        <div className="footer_cover">
          <div className="footer_logo">
            <img
              src="https://www.bhdstar.vn/wp-content/themes/bhd/assets/images/logo.png"
              alt=""
            />
          </div>
          <div className="flex">
            <div className="footer_item">
              <h2 className=" text-white font-medium">VỀ BHD STAR</h2>
              <div className="custom_item"></div>
              <h4 className="footer_text cursor-pointer">Hệ Thống Rạp</h4>
              <h4 className="footer_text cursor-pointer ">Tuyển Dụng</h4>
              <h4 className="footer_text cursor-pointer">Liên Hệ</h4>
              <a href="http://online.gov.vn/HomePage/CustomWebsiteDisplay.aspx?DocId=46613">
                <img
                  src="https://www.bhdstar.vn/wp-content/uploads/2020/02/dathongbao-1.png"
                  alt=""
                  className=" w-40"
                />
              </a>
            </div>
            <div className="footer_item">
              <h2 className=" text-white font-medium">QUY ĐỊNH & ĐIỀU KHOẢN</h2>
              <div className="custom_item"></div>
              <h4 className="footer_text cursor-pointer">
                Quy định thành viên
              </h4>
              <h4 className="footer_text cursor-pointer">Điều khoản</h4>
              <h4 className="footer_text cursor-pointer">
                Hướng dẫn đặt vé trực tuyến
              </h4>
              <h4 className="footer_text cursor-pointer">
                Quy định và chính sách chung
              </h4>
              <div className="footer_item2">
                <h4 className="footer_text cursor-pointer">
                  Chính sách bảo vệ thông tin cá nhân của <br /> người tiêu dùng
                </h4>
              </div>
            </div>
          </div>
        </div>

        <div className="end_footer">
          <p className="text-center text-white">© 2015 BHD Star Cineplex</p>
        </div>
      </div>
    </footer>
  );
}
