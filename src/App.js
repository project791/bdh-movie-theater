import "./App.css";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import { userRoute } from "./routes/routes";
import SpinnerComponent from "./Components/SpinnerComponent/SpinnerComponent";
function App() {
  return (
    <div>
      <SpinnerComponent />
      <BrowserRouter>
        <Switch>
          {userRoute.map((route, index) => {
            if (route.isUseLayout) {
              return (
                <Route
                  exact={route.exact}
                  key={index}
                  path={route.path}
                  render={() => {
                    return route.component;
                  }}
                />
              );
            } else {
              return (
                <Route
                  key={index}
                  component={route.component}
                  exact={route.exact}
                />
              );
            }
          })}
        </Switch>
      </BrowserRouter>
    </div>
  );
}
export default App;
