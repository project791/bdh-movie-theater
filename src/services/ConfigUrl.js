import axios from "axios";
import { handleStartLoading } from "../REDUX/action/action";
import { storeRedux } from "../REDUX/reducer/rootReducer";
import { localStorageService } from "./LocalStorage";

export const BASE_URL = "https://movienew.cybersoft.edu.vn";

export const TOKEN_CYBERSOFT =
  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCBTw6FuZyAwNCIsIkhldEhhblN0cmluZyI6IjIwLzAyLzIwMjMiLCJIZXRIYW5UaW1lIjoiMTY3Njg1MTIwMDAwMCIsIm5iZiI6MTY1NDEwMjgwMCwiZXhwIjoxNjc2OTk4ODAwfQ.QYLXMgjth5hQh9opZbNS7JEDPZGWA3o_95kR_VyLix8";
export const httpsService = axios.create({
  baseURL: BASE_URL,
  headers: {
    TokenCybersoft: TOKEN_CYBERSOFT,
    Authorization: "Bearer " + localStorageService?.getUserInfor()?.accessToken,
  },
});

httpsService.interceptors.request.use(
  function (config) {
    storeRedux.dispatch(handleStartLoading(true));
    return config;
  },
  function (error) {
    return Promise.reject(error);
  }
);
httpsService.interceptors.response.use(
  //then
  function (response) {
    storeRedux.dispatch(handleStartLoading(false));
    return response;
  },
  //catch
  function (error) {
    storeRedux.dispatch(handleStartLoading(false));
    // if (error.response.status == 403) {
    //   window.location.href = "/dangnhap";
    // }
    // switch (error.response.status) {
    //   case 401:
    //   case 403:
    //     window.location.href = "/";
    // }
    // return Promise.reject(error);
  }
);
// ! không dùng đc useDispatch ,  ko có hook
