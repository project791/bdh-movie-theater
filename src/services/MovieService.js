import { httpsService } from "./ConfigUrl";
import { localStorageService } from "./LocalStorage";
export const movieService = {
  getBannerMovie: () => {
    return httpsService.get("/api/QuanLyPhim/LayDanhSachBanner");
  },
  getMovieList: () => {
    return httpsService.get("/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP07");
  },
  getDetailMovie: (maPhim) => {
    return httpsService.get(`/api/QuanLyPhim/LayThongTinPhim?MaPhim=${maPhim}`);
  },
  getThongTinLichChieu: (maPhim) => {
    return httpsService.get(
      `/api/QuanLyRap/LayThongTinLichChieuPhim?MaPhim=${maPhim}`
    );
  },
  getDanhSachPhongVe: (maLichChieu) => {
    return httpsService.get(
      `/api/QuanLyDatVe/LayDanhSachPhongVe?MaLichChieu=${maLichChieu}`
    );
  },
  postDatVe: (thongTinGhe) => {
    httpsService.defaults.headers.post["Authorization"] =
      "Bearer " + localStorageService?.getUserInfor()?.accessToken;
    return httpsService.post("/api/QuanLyDatVe/DatVe", thongTinGhe);
  },
};
