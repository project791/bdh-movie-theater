const USER = "user";
export const localStorageService = {
  setUserInfor: (inforUser) => {
    localStorage.setItem(USER, JSON.stringify(inforUser));
  },
  getUserInfor: () => {
    let dataJson = localStorage.getItem(USER);
    if (dataJson) {
      return JSON.parse(dataJson);
    }
    return null;
  },
  removeUserInfor: () => {
    localStorage.removeItem(USER);
  },
};
