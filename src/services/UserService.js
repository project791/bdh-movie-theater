import axios from "axios";
import { BASE_URL, httpsService, TOKEN_CYBERSOFT } from "./ConfigUrl";
export const userService = {
  postDangNhap: (dataLogin) => {
    return httpsService.post("/api/QuanLyNguoiDung/DangNhap", dataLogin);
  },
  postDangKy: (dataSignUp) => {
    return httpsService.post("/api/QuanLyNguoiDung/DangKy", dataSignUp);
  },
};
