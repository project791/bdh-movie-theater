import React from "react";
import ReactDOM from "react-dom/client";
import "react-modal-video/css/modal-video.css";
import "antd/dist/antd.css";
import "./index.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import { Provider } from "react-redux";
import { storeRedux } from "./REDUX/reducer/rootReducer";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <Provider store={storeRedux}>
    <App />
  </Provider>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
